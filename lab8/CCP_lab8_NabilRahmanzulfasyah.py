class KueUlangTahun:
    def __init__(self, tipe, harga, tulisan, angka_lilin, topping):
        self.tipe = tipe
        self.harga = harga
        self.tulisan = tulisan
        self.angka_lilin = angka_lilin
        self.topping = topping
    
    def get_tipe(self):
        # TODO: Implementasikan getter untuk tipe!
        pass

    def get_harga(self):
        # TODO: Implementasikan getter untuk harga!
        pass
    
    def get_tulisan(self):
        # TODO: Implementasikan getter untuk tulisan!
        pass
    
    def get_angka_lilin(self):
        # TODO: Implementasikan getter untuk angka_lilin!
        pass
    
    def get_topping(self):
        # TODO: Implementasikan getter untuk topping!
        pass

class KueSponge(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, rasa, warna_frosting, harga = 2500):
        # TODO: Implementasikan constructor untuk class ini!
        super().__init__(tulisan, angka_lilin, topping)
        self.rasa = rasa
        self.warna_frosting = warna_frosting
        self.harga = harga
    
    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!
    def get_rasa(self):
        print(self.rasa)

    def get_warna_frosting(self):
        print(self.warna_frosting)

    def get_harga(self):
        print(self.harga)
    
class KueKeju(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, jenis_kue_keju, harga=3000):
        # TODO: Implementasikan constructor untuk class ini!
        super().__init__(tulisan, angka_lilin, topping)
        self.jenis_kue_keju = jenis_kue_keju
        self.harga = harga

    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!
    def get_jenis_kue_keju(self):
        pass

    def get_harga(self):
        pass
    
class KueBuah(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, jenis_kue_buah, harga=3500):
        super().__init__(tulisan, angka_lilin, topping)
        self.jenis_kue_buah = jenis_kue_buah
        self.harga = harga
        
    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!
    def get_jenis_kue_keju(self):
        pass

    def get_harga(self):
        pass
    
# Fungsi ini harus diimplementasikan!
def buat_custom_bundle():
    # TODO: Implementasikan menu untuk membuat custom bundle!
    pass

# Fungsi ini harus diimplementasikan!
def pilih_premade_bundle():
    # TODO: Implementasikan menu untuk memilih premade bundle!
    pass

# Fungsi ini harus diimplementasikan!
def print_detail_kue(kue):
    # TODO: Implementasikan kode untuk print detail dari suatu kue!
    print("berikut adalah pesanan anda:")
    print(kue.tipe)
    print()
    pass

# Fungsi main jangan diubah!
def main():
    print("Selamat datang di Homura!")
    print("Saat ini sedang diadakan event khusus bertema kue ulang tahun.")

    is_ganti = True

    while is_ganti:
        print("\nBundle kue yang kami sediakan: ")
        print("1. Bundle pre-made")
        print("2. Bundle custom\n")

        pilihan_bundle = input("Pilih bundle: ")

        kue = None

        while True:
            if pilihan_bundle == "1":
                kue = pilih_premade_bundle()
                break
            elif pilihan_bundle == "2":
                kue = buat_custom_bundle()
                break
            else:
                print("Pilihan anda tidak valid.")
                pilihan_bundle = input("Pilih paket: ")
        
        print("\nBerikut adalah kue pesanan anda: ")

        print_detail_kue(kue)

        while True:
            ganti = input("Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")

            if ganti == "Ya":
                break
            elif ganti == "Tidak":
                is_ganti = False
                break
            else:
                print("Pilihan anda tidak valid.")
                ganti = input("Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")
    
    print("\nTerima kasih sudah berbelanja di Homura!")

if __name__ == "__main__":
    main()